## About

Kutt is a modern URL shortener with support for custom domains. Shorten URLs, manage your links and view the click rate statistics.

## Features

* Free and open source.
* Custom domain support.
* Custom URLs for shortened links
* Set password for links.
* Set description for links.
* Expiration time for links.
* Private statistics for shortened URLs.
* View, edit, delete and manage your links.
* Admin account to view, delete and ban links.
* Ability to disable registration and anonymous link creation for private use.
* RESTful API.


