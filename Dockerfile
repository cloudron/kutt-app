FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

# https://github.com/thedevs-network/kutt/blob/develop/Dockerfile
ARG NODE_VERSION=22.13.0
RUN mkdir -p /usr/local/node-${NODE_VERSION} && \
    curl -L https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-${NODE_VERSION}
ENV PATH /usr/local/node-${NODE_VERSION}/bin:$PATH

# renovate: datasource=github-releases depName=thedevs-network/kutt versioning=semver extractVersion=^v(?<version>.+)$
ARG KUTT_VERSION=3.2.3

RUN curl -L https://github.com/thedevs-network/kutt/archive/v${KUTT_VERSION}.tar.gz | tar -xz --strip-components 1 -f - && \
    ln -s /run/kutt/logs /app/code/logs && \
    ln -s /app/data/env /app/code/.env && \
    rm -rf /app/code/custom && ln -s /app/data/custom /app/code/custom

RUN npm ci --omit=dev

ENV NODE_ENV=production

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
