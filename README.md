# Kutt Cloudron App

This repository contains the Cloudron app package source for [Kutt](https://kutt.it)

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=it.kutt.cloudronappp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id it.kutt.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd kutt-app

cloudron build
cloudron install
```

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/).

```
cd kutt-app/test

npm install
USERNAME=<cloudron username> PASSWORD=<cloudron password> mocha --bail test.js
```

