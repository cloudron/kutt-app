#!/bin/bash

set -eu

mkdir -p /run/kutt/logs /app/data/custom

if [[ ! -f /app/data/env ]]; then
    echo "==> First run. Creating env"
    cp /app/code/.example.env /app/data/env

    sed -e "s/^DISALLOW_REGISTRATION=.*/DISALLOW_REGISTRATION=true/" \
        -e "s/^DISALLOW_ANONYMOUS_LINKS=.*/DISALLOW_ANONYMOUS_LINKS=true/" \
        -e "s/^JWT_SECRET=.*/JWT_SECRET=$(pwgen -1s 32)/" \
        -i /app/data/env
fi

sed -e "s/^PORT=.*/PORT=3000/" \
    -e "s/^DEFAULT_DOMAIN=.*/DEFAULT_DOMAIN=${CLOUDRON_APP_DOMAIN}/" \
    -e "s/^CUSTOM_DOMAIN_USE_HTTPS=.*/CUSTOM_DOMAIN_USE_HTTPS=true/" \
    -e "s/^DB_CLIENT=.*/DB_CLIENT=pg/" \
    -e "s/^DB_HOST=.*/DB_HOST=${CLOUDRON_POSTGRESQL_HOST}/" \
    -e "s/^DB_PORT=.*/DB_PORT=${CLOUDRON_POSTGRESQL_PORT}/" \
    -e "s/^DB_NAME=.*/DB_NAME=${CLOUDRON_POSTGRESQL_DATABASE}/" \
    -e "s/^DB_USER=.*/DB_USER=${CLOUDRON_POSTGRESQL_USERNAME}/" \
    -e "s/^DB_PASSWORD=.*/DB_PASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD}/" \
    -e "s/^DB_SSL=.*/DB_SSL=false/" \
    -e "s/^REDIS_HOST=.*/REDIS_HOST=${CLOUDRON_REDIS_HOST}/" \
    -e "s/^REDIS_PORT=.*/REDIS_PORT=${CLOUDRON_REDIS_PORT}/" \
    -e "s/^REDIS_PASSWORD=.*/REDIS_PASSWORD=${CLOUDRON_REDIS_PASSWORD}/" \
    -e "s/^MAIL_ENABLED=.*/MAIL_ENABLED=true/" \
    -e "s/^MAIL_HOST=.*/MAIL_HOST=${CLOUDRON_MAIL_SMTP_SERVER}/" \
    -e "s/^MAIL_PORT=.*/MAIL_PORT=${CLOUDRON_MAIL_SMTP_PORT}/" \
    -e "s/^MAIL_SECURE=.*/MAIL_SECURE=false/" \
    -e "s/^MAIL_USER=.*/MAIL_USER=${CLOUDRON_MAIL_SMTP_USERNAME}/" \
    -e "s/^MAIL_PASSWORD=.*/MAIL_PASSWORD=${CLOUDRON_MAIL_SMTP_PASSWORD}/" \
    -e "s/^MAIL_FROM=.*/MAIL_FROM=${CLOUDRON_MAIL_FROM}/" \
    -e "s/^TRUST_PROXY=.*/TRUST_PROXY=true/" \
    -i /app/data/env

echo "==> Changing ownership"
chown -R cloudron:cloudron /app/data /run/kutt

# https://babeljs.io/docs/en/babel-register/
export BABEL_DISABLE_CACHE=1

echo "==> Running migrations"
gosu cloudron:cloudron npm run migrate

echo "==> Starting Kutt"
exec gosu cloudron:cloudron npm start

